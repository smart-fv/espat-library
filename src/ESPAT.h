#ifndef ESPAT_H
#define ESPAT_H
 
#include "mbed.h"
#include <string> 
 
class ESPAT {
public:
    ESPAT(PinName tx, PinName rx, int baud = 115200);
    ESPAT();
    void resetEsp();
    void initWifiStation(string ssid, string pwd);
    void initWifiAP(string ssid = "ESPAT", string pwd = "12345678", string channel = "8", string encryption = "3");
    void initServer(void (*requestHandler)(int, string));    
    void tcpReply(int linkId, string header, string payload);
    void httpReply(int linkId, string code, string payload);    
private:
    BufferedSerial espSerial;
    string wifiName;
    string wifiPass;
    unsigned char ntpReq[48];
    unsigned char buffer[100];
    void readStrUntil(string * str, char until);
    void waitFor(char * text);
    void sendCommand(char * cmd, bool waitOk = true);
};
 
#endif
#include "ESPAT.h"
#include <cstring>

ESPAT::ESPAT(PinName tx, PinName rx, int baud) : espSerial(tx, rx, baud) {
}

void ESPAT::readStrUntil(string * str, char until) {
    char c[10];
    while (true) {
        espSerial.read((void*)c, 1);
        if (c[0] != until) {
            *str += c;
        } else {
            break;
        }
    }
}

void ESPAT::waitFor(char * text) {
    //string s;
    char c[10];
    for (int i = 0; i < strlen(text); i++) {
        espSerial.read((void*)c, 1);
        //s += c;
        if (c[0] != text[i]) {            
            i = 0;
        }        
    }    
    //pc.printf("%s", s.c_str());
}

void ESPAT::sendCommand(char * cmd, bool waitOk) {
    //pc.printf("%s\r\n", cmd);
    char buffer[100];
    strcpy(buffer, cmd);
    strcat(buffer, "\r\n");
    espSerial.write((void*)buffer, strlen(buffer));
    if (waitOk) waitFor((char*)"OK");
}

void ESPAT::resetEsp() {
    sendCommand((char*)"AT+RST");
    waitFor((char*)"ready");
    // printf("Reset effettuato\r\n");
}

void ESPAT::initWifiStation(string ssid, string pwd) {
    sendCommand((char*)"AT+CWMODE_CUR=1");
    printf("Modalità stazione impostata\r\n");
    string wifiCmd = "AT+CWJAP_CUR=\"" + ssid + "\",\"" + pwd + "\"";
    sendCommand((char*) wifiCmd.c_str());
    sendCommand((char*)"AT+CIPSTATUS", false);
    waitFor((char*)"STATUS:2");    
    printf("Connesso\r\n");
    sendCommand((char*)"AT+CIPMUX=1");
    printf("Connessione multipla impostata\r\n");
}

void ESPAT::initWifiAP(string ssid, string pwd, string channel, string encryption) {
    sendCommand((char*)"AT+CWMODE_CUR=2");
    string wifiCmd = "AT+CWSAP_CUR=\"" + ssid + "\",\"" + pwd + "\"," + channel + "," + encryption;
    sendCommand((char*) wifiCmd.c_str());
}

void ESPAT::initServer(void (*requestHandler)(int, string)) {  
    sendCommand((char*)"AT+CIPMUX=1");
    sendCommand((char*)"AT+CIPSERVER=1,80");
    while (true) {        
        waitFor((char*)"+IPD,"); 
        // Parse linkId
        int linkId = 0;
        char c[10];
        while (true) {
            espSerial.read((void*)c, 1);
            if (c[0] == ',') break;
            linkId = linkId * 10 + (c[0] - '0');
        }
        // Parse path
        waitFor((char*)":GET ");
        string path;
        readStrUntil(&path, ' ');
        // Send request to handler 
        requestHandler(linkId, path); 
    }
}

void ESPAT::tcpReply(int linkId, string header, string payload) {
    char buffer[20];    
    int len = header.length();   
    int rep = 0;   

    sprintf(buffer, "AT+CIPSEND=%d,%d\r\n", linkId, len);
    espSerial.write((void*)buffer, strlen(buffer));
    thread_sleep_for(100); // Can't use waitFor here as it isn't fast enough
    char c[10];
    for (int i = 0; i < len; i++) {
        // Artificially slowing down this to prevent errors
        c[0] = header[i];
        espSerial.write((void*)c, 1);
    }
    waitFor((char*)"SEND OK");    

    len = payload.length();
    string packet;
    int k;
    while (len > 2000) { 
        packet = "7D0\r\n";       
        packet += payload.substr(0, 2000);
        packet += "\r\n";
        payload.erase(0, 2000);
        len -= 2000;
        k = packet.length();
        sprintf(buffer, "AT+CIPSEND=%d,%d\r\n", linkId, k);
        espSerial.write((void*)buffer, strlen(buffer));
        thread_sleep_for(100); // Can't use waitFor here as it isn't fast enough        
        for (int i = 0; i < k; i++) {
            // Artificially slowing down this to prevent errors
            c[0] = packet[i];
            espSerial.write((void*)c, 1);
        }
        waitFor((char*)"SEND OK");        
    }
    char hexLen[5];
    sprintf(hexLen, "%X", len);
    packet.assign(hexLen);
    packet += "\r\n";    
    packet += payload; 
    packet += "\r\n";
    k = packet.length();
    sprintf(buffer, "AT+CIPSEND=%d,%d\r\n", linkId, k);
    espSerial.write((void*)buffer, strlen(buffer));
    thread_sleep_for(100); // Can't use waitFor here as it isn't fast enough        
    for (int i = 0; i < k; i++) {
        // Artificially slowing down this to prevent errors
        c[0] = packet[i];
        espSerial.write((void*)c, 1);
    }
    waitFor((char*)"SEND OK"); 
    packet = "0\r\n\r\n";
    k = packet.length(); 
    sprintf(buffer, "AT+CIPSEND=%d,%d\r\n", linkId, k);
    espSerial.write((void*)buffer, strlen(buffer));
    thread_sleep_for(100); // Can't use waitFor here as it isn't fast enough        
    for (int i = 0; i < k; i++) {
        // Artificially slowing down this to prevent errors
        c[0] = packet[i];
        espSerial.write((void*)c, 1);
    }
    waitFor((char*)"SEND OK");  
    sprintf(buffer, "AT+CIPCLOSE=%d\r\n", linkId);
    espSerial.write((void*)buffer, strlen(buffer));
    waitFor((char*)"OK");
}

void ESPAT::httpReply(int linkId, string code, string payload) {
    string header = "HTTP/1.1 " + code + "\r\nContent-Type: text/html\r\nTransfer-Encoding: chunked\r\n\r\n";
    tcpReply(linkId, header, payload);
}